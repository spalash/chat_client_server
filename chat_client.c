#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define BUF_SIZE 500

int
main(int argc, char *argv[])
{
   struct addrinfo hints;
   struct addrinfo *result, *rp;
   int sfd, multi_cast_sfd, s, j;
   size_t len;
   ssize_t nread;
   char buf[BUF_SIZE];

   if (argc < 2) {
       fprintf(stderr, "Usage: %s host port ...\n", argv[0]);
       exit(EXIT_FAILURE);
   }

   /* Obtain address(es) matching host/port */

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
   hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
   hints.ai_flags = 0;
   hints.ai_protocol = 0;          /* Any protocol */
   
   s = getaddrinfo(argv[1], argv[2], &hints, &result);
   if (s != 0) {
       fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
       exit(EXIT_FAILURE);
   }

   /* getaddrinfo() returns a list of address structures.
      Try each address until we successfully connect(2).
      If socket(2) (or connect(2)) fails, we (close the socket
      and) try the next address. */

   for (rp = result; rp != NULL; rp = rp->ai_next) {
       sfd = socket(rp->ai_family, rp->ai_socktype,
                    rp->ai_protocol);
       if (sfd == -1)
           continue;

       if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
           break;                  /* Success */

       close(sfd);
   }

   if (rp == NULL) {               /* No address succeeded */
       fprintf(stderr, "Could not connect\n");
       exit(EXIT_FAILURE);
   }

   // hints.ai_family = AF_INET6; 
   // hints.ai_flags = AI_PASSIVE;
   // hints.ai_canonname = NULL;
   // hints.ai_addr = NULL;
   // hints.ai_next = NULL;

   // s = getaddrinfo("FFFF:0:0:0:0:0:0:0", argv[4], &hints, &result);
   // if (s != 0) {
   //     fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
   //     exit(EXIT_FAILURE);
   // }

   //  getaddrinfo() returns a list of address structures.
   //    Try each address until we successfully connect(2).
   //    If socket(2) (or connect(2)) fails, we (close the socket
   //    and) try the next address. 

   // for (rp = result; rp != NULL; rp = rp->ai_next) {
   //     multi_cast_sfd = socket(rp->ai_family, rp->ai_socktype,
   //                  rp->ai_protocol);
   //     if (multi_cast_sfd == -1)
   //         continue;

   //     if (connect(multi_cast_sfd, rp->ai_addr, rp->ai_addrlen) != -1)
   //         break;                  /* Success */

   //     close(multi_cast_sfd);
   // }

   // if (rp == NULL) {               /* No address succeeded */
   //     fprintf(stderr, "11Could not connect\n");
   //     exit(EXIT_FAILURE);
   // }

   freeaddrinfo(result);           /* No longer needed */

	printf("client: ");fflush(stdout);
	fgets(buf, BUF_SIZE, stdin);
	if (write(sfd, buf, BUF_SIZE) != BUF_SIZE) { fprintf(stderr, "partial/failed write\n");
		exit(EXIT_FAILURE);
	}
	while(strcmp(buf,"exit\n")){				
		memset(buf,0,BUF_SIZE);
		printf("client: "); fflush(stdout);
		memset(buf,0,BUF_SIZE);				
		fgets(buf, BUF_SIZE, stdin);
		if (write(sfd, buf, BUF_SIZE) != BUF_SIZE) { fprintf(stderr, "partial/failed write\n");
		   exit(EXIT_FAILURE);
		}
   }
	close(sfd);
   exit(EXIT_SUCCESS);
}