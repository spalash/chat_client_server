#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <pthread.h>


#define BUF_SIZE 500

void* chat_func(void *argv){
	char buf[BUF_SIZE];
	ssize_t nread;
	int  connfd = *((int*)argv), id=*(((int*)argv)+1);
	
    nread = recv(connfd, buf, BUF_SIZE, 0);
	if (nread == -1){
		fprintf(stderr, "Error recieving messages\n");	   
		return;
	}
	while(strcmp(buf,"exit\n")){
		printf("client %d : %s", id,buf);fflush(stdout);
		memset(buf,0,BUF_SIZE);
		memset(buf,0,BUF_SIZE);
		nread = recv(connfd, buf, BUF_SIZE, 0);
		if (nread == -1){
		   fprintf(stderr, "Error recieving messages\n");	   
		   break;		
		}
	}
	printf("Client %d offline!\n",id);
}

int main(int argc, char *argv[])
{
   struct addrinfo hints;
   struct addrinfo *result, *rp;
   int sfd, multi_cast_sfd, s, chat_arg[2];
   struct sockaddr_storage peer_addr;
   socklen_t peer_addr_len;
   pthread_t tid;

   if (argc < 2) {
       fprintf(stderr, "Usage: %s port\n", argv[0]);
       exit(EXIT_FAILURE);
   }

   memset(&hints, 0, sizeof(struct addrinfo));
   hints.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
   hints.ai_socktype = SOCK_STREAM; /* TCP socket */
   hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
   hints.ai_protocol = 0;          /* Any protocol */
   hints.ai_canonname = NULL;
   hints.ai_addr = NULL;
   hints.ai_next = NULL;

   s = getaddrinfo(NULL, argv[1], &hints, &result);
   if (s != 0) {
       fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
       exit(EXIT_FAILURE);
   }

   /* getaddrinfo() returns a list of address structures.
   Try each address until we successfully bind(2).
      If socket(2) (or bind(2)) fails, we (close the socket
      and) try the next address. */

   for (rp = result; rp != NULL; rp = rp->ai_next) {
       sfd = socket(rp->ai_family, rp->ai_socktype,
               rp->ai_protocol);
       if (sfd == -1)
           continue;

       if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
           break;                  /* Success */

       close(sfd);
   }

   if (rp == NULL) {               /* No address succeeded */
       fprintf(stderr, "Could not bind\n");
       exit(EXIT_FAILURE);
   }

    hints.ai_family = AF_INET6;
    hints.ai_flags = 0;    
    
   //  s = getaddrinfo("FFFF:0:0:0:0:0:0:0", argv[2], &hints, &result);
   // if (s != 0) {
   //     fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
   //     exit(EXIT_FAILURE);
   // }

   // /* getaddrinfo() returns a list of address structures.
   //    Try each address until we successfully connect(2).
   //    If socket(2) (or connect(2)) fails, we (close the socket
   //    and) try the next address. */

   // for (rp = result; rp != NULL; rp = rp->ai_next) {
   //     multi_cast_sfd = socket(rp->ai_family, rp->ai_socktype,
   //                  rp->ai_protocol);
   //     if (multi_cast_sfd == -1)
   //         continue;

   //     if (connect(multi_cast_sfd, rp->ai_addr, rp->ai_addrlen) != -1)
   //         break;                  /* Success */

   //     close(multi_cast_sfd);
   // }

   // if (rp == NULL) {               /* No address succeeded */
   //     fprintf(stderr, "11Could not connect\n");
   //     exit(EXIT_FAILURE);
   // }    

   freeaddrinfo(result);           /* No longer needed */

    listen(sfd,1024);

    chat_arg[1]=0;
    while(chat_arg[0] = accept(sfd,(struct sockaddr *) &peer_addr,&peer_addr_len)){
    	pthread_create(&tid, NULL, chat_func, (void*)chat_arg);
    	chat_arg[1]++;
    }

    close(sfd);
	exit(EXIT_SUCCESS);

   /* Read datagrams and echo them back to sender */
   	

}

